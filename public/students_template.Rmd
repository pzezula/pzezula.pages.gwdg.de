---
title: "Übungszettel Vorlage"
output: html_document
---

# Pakete laden

Laden Sie in diesem Code-Chunk die Pakete, die sie benötigen. 
**Masking:** 
Manche Pakete definieren dieselben Namen (Funktionen) wie andere Pakete. Hier entscheidet die Reihenfolge des Ladens, wo das Objekt gefunden und ggf. ausgeführt wird. Eine Möglichkeit, sich vor Masking-Effekten zu schützen, ist, alle Pakete am Anfang eines RMD zu laden.```{r}

```


# Arbeitsverzeichnis setzen
Setzen Sie in diesem Code-Chunk das Arbeitsverzeichnis für diesen Übungszettel. **Setzen Sie das Arbeitsverzeichnis immer am Anfang der Datei** (vor oder nach den Paketen ist nicht wichtig.)
```{r}

```

# Aufgabe 1

Hier kommt Text hin.

```{r}
# Hier kommt Code hin.
```

Hier kann auch Text hin.

# Aufgabe 2

Hier kommt Text hin.

```{r}
# Hier kommt Code hin.
```

# Aufgabe 3

Hier kommt Text hin.

```{r}
# Hier kommt Code hin.
```

