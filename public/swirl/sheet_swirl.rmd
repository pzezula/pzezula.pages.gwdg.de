---
title: "R-Workshop für Zuhause - Swirl"
subtitle: "M.Psy.205, Sommersemester 2019, Dozent: Dr. Peter Zezula"
author: "Birk Oelhoff (birksvante.oelhoff@stud.uni-goettingen.de)"
output:
  html_document:
    theme: paper
    toc: yes
    toc_depth: 3
    toc_float: yes
  pdf_document:
    latex_engine: xelatex
header-includes:
 - \usepackage{comment}
params:
 soln: TRUE
mainfont: Helvetica
linkcolor: blue
---


### [Rmd](sheet_swirl.rmd)

# Swirl 

Ein großer Vorteil von R ist, dass es eine riesige Sammlung von Packages gibt, die kostenlos installiert und benutzt werden können. Wir werde eine Reihe von Packages kennenlernen. Ein interessantes Paket ist 'Swirl'. Swirl macht es möglich, dass Sie in Ihrem R einen interaktiven Kurs machen können. Sie lernen also R direkt in R. Wir haben für Sie einen kleinen Kurs mit mehreren Einheiten erstellt. Wir hoffen, dass Ihnen dieser den Einstieg in R erleichtert. 

## Installation

Zuerst müssen Sie das Package installieren. Wenn Sie im Computerpool arbeiten oder über die Remote-Desktopverbindung, ist dieser Schritt nicht notwendig.
Hier prüfen wir, ob das Package bereits installiert ist und installieren es nur, wenn es noch nicht da ist.

```{r eval=F}
# install.packages("swirl")
# we check, whether swirl is installed. If not, we install it.
if (!require("swirl")) { install.packages("swirl")}

```



## Swirl laden

Es gibt ein paar Grundpakete, die automatisch geladen werden. Alle anderen Pakete müssen wir erst aus unserer Sammlung laden, um sie verwenden zu können (so auch Swirl). Sie können den Code einfach in Ihre Konsole kopieren und ausführen. 
```{r}
# library("swirl")
# better require("swirl"), it checks, whether swirl is loaded and loads it if not
require("swirl")
```

## Sprache ändern

Swirl ist standardmäßig in Englisch. Wenn Sie mögen, können Sie die Sprache aber auch auf Deutsch stellen. 
```{r eval=F}
select_language("german")
# or any other language of: "chinese_simplified". "english", "french", "german", "korean", "spanish", or "turkish"
# if you leave parameters empty, you get a menu
# select_language()
```

## Kurs installieren

Der Kurs ist zu finden unter:
[swirl_r_workshop_for_home.swc](swirl_r_workshop_for_home.swc)

Alternativ liegt er in meiner 
[Dropbox](https://www.dropbox.com/s/6tkif7puk8gwxxo/R-Workshop_for_home.zip?dl=0) 
bzw. in meiner 
[OwnCloud](https://owncloud.gwdg.de/index.php/s/04bEZGa1zyFnidY)

swirl_r_workshop_for_home.swc
raq_data <- read_delim("data/raq.dat", delim = "\t")

### Kurs automatisch installieren

Als nächstes müssen wir den Kurs installieren. Wenn Sie folgenden Code ausführen, wird alles automatisch installiert. Swirl greift auf die Datei in meiner Dropbox zu und installiert den Kurs für Sie.
```{r eval=F}
install_course_dropbox("https://www.dropbox.com/s/6tkif7puk8gwxxo/R-Workshop_for_home.zip?dl=0")
```

### Kurs manuell installieren

Sie können den Kurs auch manuell installieren. Sie können dazu eine [swc-Datei hier herunterladen](https://owncloud.gwdg.de/index.php/s/04bEZGa1zyFnidY). Dieses Format beinhaltet alle Inhalte in einer Datei. Sie können mit folgendem Code die Datei auswählen. Es sollte sich ein Fenster öffnen mit dem Sie die Datei auswählen können. Der Pfad wird dann unter x abgespeichert. Wenn Sie den Pfad manuell festlegen wollen, können Sie diesen Schritt überspringen.
```{r eval=F}
x <- choose.files()
```

Als nächstes müssen wir den Kurs installieren und Swirl sagen, wo die swc-Datei zu finden ist. Den Pfad zur Datei haben wir eben unter x gespeichert. Sie müssen also nur folgendes ausführen:
```{r eval=F}
install_course(swc_path = x)
```





## Swirl starten

Mit dem Befehl `swirl()` starten Sie Swirl. Ab jetzt erklärt Ihnen Swirl eigentlich alles. Zuerst fragt Sie Swirl nach Ihrem Namen. Tippen Sie ruhig wirklich Ihren Namen ein. Dadurch können Sie den Kurs jederzeit dort weiterführen, wo Sie letztes mal aufgehört haben. Wenn Sie dann einen anderen Namen eintippen, denkt Swirl, dass Sie eine andere Person sind. 

```{r}
info() #zeigt die Befehle, die Sie swirl geben können
```

## Kurs auswählen

Sie sollten jetzt den *R-Workshop for home* sehen können. Wählen Sie diesen aus und los geht's! Swirl zeigt Ihnen auch wie Sie weitere Kurse im Repository finden können.

# Feedback

Wir sind sehr gespannt auf Ihr Feedback zu diesem Kurs. Sie können [hier auf ein Google Dokument zugreifen.](https://docs.google.com/document/d/1IFIzwOyCE1ahm1eLWZmZwmtKkTatLO1Nkrp1mSwZgeE/edit?usp=sharing) Schreiben Sie gerne Anregungen, Wünsche, Verbesserungsvorschläge oder auch was Ihnen gefallen hat. Danke! 


### hier ein paar noch nicht lauffähige Versuche

bitte nicht beachten

```{r eval=F}
# install_course_url('https://pzezula.pages.gwdg.de/swirl/swirl/R-Workshop_for_home.zip')
# install_course(swc_path = "https://pzezula.pages.gwdg.de/swirl/swirl/r-workshop_for_home.swc")
# install_course(swc_path = "/Users/pzezula/pzezula.pages.gwdg.de/public/swirl/r-workshop_for_home.swc")
```





